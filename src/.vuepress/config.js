module.exports = {
  title: "FediWiki",
  themeConfig: {
    nav: [
      { text: 'Guide', link: '/s/' }
    ],
    sidebar: [["/", "Home"],["/Decentralization/", "Decentralization"]]
  },
  postcss: {
    plugins: [
      require("tailwindcss")("./tailwind.config.js"),
      require("autoprefixer")
    ]
  }
};