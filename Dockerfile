FROM node:12.6.0 AS builder
WORKDIR .
COPY . .
RUN yarn install
RUN yarn build

FROM nginx:1.17.1
COPY --from=builder src/.vuepress/dist /usr/share/nginx/html
